execute pathogen#infect()
set t_Co=256
syntax on
set number
filetype plugin indent on
let g:solarized_termcolors=256
let g:solarized_termtrans=1
set background=dark
colorscheme solarized
let g:airline_powerline_fonts=1
set laststatus=2
let g:airline_theme='solarized'
highlight LineNr ctermbg=073642
call togglebg#map("<F5>")
"if &background == "dark"
"	let s:base03 = "NONE"
"	let s:base02 = "NONE"
"endif
set tabstop=2
set shiftwidth=2
set smarttab
set expandtab
set smartindent
let g:flow#enable=0
let g:javascript_plugin_flow=1
